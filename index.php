<?php
header("Access-Control-Allow-Origin: *");

$customer_ID = $_POST['getCustomerID'];
$subscription_ID = $_POST['getSubscriptionID'];

//echo "Customer ID IS =", $customer_ID, "Subscription ID is =", $subscription_ID;

//API Details     client id:client secret

//BETA
//$api_details = "6ba92ae84e9f571cbf1a12678633acde:46f7d00bd56a96cf527f102bd782f869";

//LIVE
$api_details = "e3b6d8256d99b9770ff02d4caeb169a7:c628a8e1d8655c01b3f571c2c0e02617";

$ch = curl_init();
$url = 'https://api.cratejoy.com/v1/customers/' . $customer_ID . '/';
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_USERPWD, $api_details);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  "Content-Type: application/json"
));
$response = curl_exec($ch);
curl_close($ch);


if (strpos($response, 'Customer not found') !== false) {
  echo "There was an error identifiying the validity of your account, please contact customer service on <a href='mailto:papergang@ohhdeer.com'>papergang@ohhdeer.com</a>.";
} else {
  $ch1 = curl_init();
  $data1 = '{
    "coupon_id": "1920992027"
  }';
  $url1 = 'https://api.cratejoy.com/v1/subscriptions/' . $subscription_ID . '/coupons/';
  curl_setopt($ch1, CURLOPT_URL, $url1);
  curl_setopt($ch1, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch1, CURLOPT_HEADER, FALSE);
  curl_setopt($ch1, CURLOPT_USERPWD, $api_details);
  curl_setopt($ch1, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json"
  ));
  curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, 'POST');
  curl_setopt($ch1, CURLOPT_POSTFIELDS, $data1);
  $response1 = curl_exec($ch1);
  curl_close($ch1);

  //var_dump($response1);
  //print "\n";
  //print_r($response1);

  // Interpret Response

  $error_state = false;

  if (strpos($response1, 'Subscription already has the coupon') !== false) {
    $error_state = true;
    $error_message = "Your subscription already has a coupon applied so this one could not be added, please contact customer service on <a href='mailto:papergang@ohhdeer.com'>papergang@ohhdeer.com</a> for assistance.";
  } else if (strpos($response1, 'Subscription not found') !== false) {
    $error_state = true;
    $error_message = "Your subscription was not found on our system please contact customer service on <a href='mailto:papergang@ohhdeer.com'>papergang@ohhdeer.com</a> for assistance.";
  }  else if (strpos($response1, 'errors') !== false) {
    $error_state = true;
    $error_message = "There was a problem applying your offer, please contact customer service on <a href='mailto:papergang@ohhdeer.com'>papergang@ohhdeer.com</a> and they'll be able to help you out.";
  } else {
    $error_state = false;
  }

  if ($error_state === true) {
    echo $error_message;
  } else {
    echo "Offer applied, your next renewal will reflect this offer.";
  }

  //  if (strpos($response1, 'errors') !== false) {
  //    echo "There was an error adding the offer to your subscription, please contact customer server on papergang@ohhdeer.com<br> Please quote the following error to customer service:<i>", $response1,"</i>";
  //  } else {
  //    echo "Offer applied, your next renewal will reflect this offer.";
  //  }

}
?>
